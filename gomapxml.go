package gomapxml

type PortsUsed struct {
    State string `xml:"state,attr"`
    Proto string `xml:"proto,attr"`
    Port uint32 `xml:"portid,attr"`
}

type OSClass struct {
    Type string `xml:"type,attr"`
    Vendor string `xml:"vendor,attr"`
    Family string `xml:"osfamily,attr"`
    Gen string `xml:"osgen,attr"`
    Accuracy int `xml:"accuracy,attr"`
}

type OSMatch struct {
    Name string `xml:"name,attr"`
    Accuracy int `xml:"accuracy,attr"`
    Line uint32 `xml:"line,attr"`
    Class []OSClass `xml:"osclass"`
}

type OS struct {
    Ports []PortsUsed `xml:"portused"`
    Type OSMatch `xml:"osmatch"`
}

type Service struct {
    Name string `xml:"name,attr"`
    Product string `xml:"product,attr"`
    Version string `xml:"version,attr"`
    Extra string `xml:"extrainfo,attr"`
    Device string `xml:"devicetype,attr"`
    Method string `xml:"method,attr"`
    Conf int `xml:"conf,attr"`
}

type Status struct {
    State string `xml:"state,attr"`
    Reason string `xml:"reason,attr"`
    TTL int `xml:"reason_ttl,attr"`
}

type Address struct {
    Addr string `xml:"addr,attr"`
    Type string `xml:"addrtype,attr"`
    Vendor string `xml:"vendor,attr"`
}

type Hostname struct {
    Name string `xml:"name,attr"`
    Type string `xml:"type,attr"`
}

type PortData struct {
    Protocol string `xml:"protocol,attr"`
    Port uint32 `xml:"portid,attr"`
    State Status `xml:"state"`
    Service Service `xml:"service"`
}

type Trace struct {
    Proto string `xml:"proto,attr"`
    Port uint32 `xml:"port,attr"`
    Hops []Hop `xml:"hop"`
}

type Hop struct {
    TTL uint8 `xml:"ttl,attr"`
    IpAddr string `xml:"ipaddr,attr"`
    RTT float32 `xml:"rtt,attr"`
    Host string `xml:"host,attr"`
}

type Host struct {
    Start uint32 `xml:"starttime,attr"`
    End uint32 `xml:"endtime,attr"`
    Addr []Address `xml:"address"`
    Hostnames []Hostname `xml:"hostnames>hostname"`
    Trace Trace  `xml:"trace"`
    Status Status `xml:"status"`
    Data []PortData `xml:"ports>port"`
    OS OS `xml:"os"`
    Uptime struct {
        Seconds uint64 `xml:"seconds,attr"`
        Boot string `xml:"lastboot,attr"`
    } `xml:"uptime"`
}

type NmapRun struct {
    Scanner string `xml:"scanner,attr"`
    Args string `xml:"args,attr"`
    Start uint64 `xml:"start,attr"`
    Startstr string `xml:"startstr,attr"`
    Version string `xml:"version,attr"`
    Xmloutputversion string `xml:"xmloutputversion,attr"`
    Scaninfo struct {
        Type string `xml:"type,attr"`
        Protocol string `xml:"protocol,attr"`
        Numservices uint16 `xml:"numservices,attr"`
        Services string `xml:"services,attr"`
    } `xml:"scaninfo"`
    Hosts []Host `xml:"host"`
}
