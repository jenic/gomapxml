# Go Nmap XML Structures

XML structures from nmap scans written for use in Go applications.

[![pipeline_status](https://gitlab.com/jenic/gomapxml/badges/master/pipeline.svg)](https://gitlab.com/jenic/gomapxml/commits/master)
