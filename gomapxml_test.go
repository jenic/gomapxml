package gomapxml

import (
    "testing"
    "encoding/xml"
    "io/ioutil"
    "os"
)

const (
    VMNET = `testdata/vmnet.xml`
    SCANME = `testdata/quicktcp_test.xml`
)

/*
func TestStreamTokens(t *testing.T) {
    // open test data
    xmlFile, err := os.Open(VMNET)
    if err != nil {
        t.Errorf("File open err: %v", err)
    }
    defer xmlFile.Close()

    decoder := xml.NewDecoder(xmlFile)

    for {
        // Read tokens from XML in a stream
        t, err := decoder.Token()
        if err != nil {
            t.Errorf("Token decode error: %v", err)
        }

        if t == nil {
            break
        }

        // inspect type of token being read
        switch se := t.(type) {
        case xml.StartElement:
            if se.Name.Local != "host" {
                continue
            }

            var p Host
            decoder.DecodeElement(&p, &se)
            t.Logf("Have: %v", p)
        default:
        }
    }
}
*/

func ReadFile(file string, t *testing.T) []byte {
    xmlFile, err := os.Open(file)
    if err != nil {
        t.Errorf("File open err: %v", err)
    }
    defer xmlFile.Close()

    data, err := ioutil.ReadAll(xmlFile)
    if err != nil {
        t.Errorf("Read err: %v", err)
    }
    return data
}

func TestUnmarshalNmapRun(t *testing.T) {
    // open test data
    xmlFile, err := os.Open(VMNET)
    if err != nil {
        t.Errorf("File open err: %v", err)
    }
    defer xmlFile.Close()

    var v NmapRun

    data, err := ioutil.ReadAll(xmlFile)
    if err != nil {
        t.Errorf("Read err: %v", err)
    }
    err = xml.Unmarshal(data, &v)
    if err != nil {
        t.Errorf("Unmarshal: %v", err)
    }

    t.Logf("NmapRun: %#v", v)
}

func TestUnmarshalValues(t *testing.T) {
    // open test data
    xmlFile, err := os.Open(VMNET)
    if err != nil {
        t.Errorf("File open err: %v", err)
    }
    defer xmlFile.Close()

    var v NmapRun

    data, err := ioutil.ReadAll(xmlFile)
    if err != nil {
        t.Errorf("Read err: %v", err)
    }
    err = xml.Unmarshal(data, &v)
    if err != nil {
        t.Errorf("Unmarshal: %v", err)
    }

    if v.Scanner != "nmap" {
        t.Errorf("Unmarshal failed! Expected value %v but got %v", `nmap`, v.Scanner)
    }

    if v.Scaninfo.Numservices != 65535 {
        t.Errorf("Scaninfo failed! Expected value %v but got %v", 65535, v.Scaninfo.Numservices)
    }
}

func TestHostnames(t *testing.T) {
    data := ReadFile(SCANME, t)

    var v NmapRun
    err := xml.Unmarshal(data, &v)
    if err != nil {
        t.Errorf("Unmarshal: %v", err)
    }

    if v.Hosts[0].Hostnames == nil {
        t.Errorf("Hostnames is nil? %v", v)
    }
}
